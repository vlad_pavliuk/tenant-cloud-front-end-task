import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fade from '@material-ui/core/Fade';
import {connect} from 'react-redux';

const spinner = (props) => (
    <Fade
        in={props.isLoading}
        unmountOnExit
    >
        <CircularProgress
            size={50}
            style={{position: 'fixed', top: '50%', left: '50%'}}
        />
    </Fade>
);

const mapStateToProps = (state) => {
    return {
        isLoading: state.settings.settings.isLoading
    }
};

export default connect(mapStateToProps)(spinner);
