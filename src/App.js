import React from 'react';
import './App.css';
import Layout from './Layout';
import './App.scss';

const App = () => {
    return (
        <div className="App">
            <Layout/>
        </div>
    );
};

export default App;
