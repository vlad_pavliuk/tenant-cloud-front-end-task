import update from 'immutability-helper';
import * as actionTypes from 'store/actions/actions';

const initialState = {
    settings: {
        theme: 'light',
        fontSize: 14,
        pageZoom: 1,
        isBookmarksShow: true,
        isLoading: false,
    },
    defaultSettings: {
        theme: 'light',
        fontSize: 14,
        pageZoom: 1,
        isBookmarksShow: true,
    },
    defaultPersonImagesUrls: [
        '/img/default-image-1.jpg',
        '/img/default-image-2.jpg',
        '/img/default-image-3.jpeg',
        '/img/default-image-4.jpeg',
        '/img/default-image-5.jpeg'
    ],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_THEME: {
            return {
                ...state,
                settings: update(state.settings, {$merge: {theme: action.payload}})
            }
        }
        case actionTypes.SET_FONT_SIZE: {
            return {
                ...state,
                settings: update(state.settings, {$merge: {fontSize: action.payload}})
            }
        }
        case actionTypes.SET_PAGE_ZOOM: {
            return {
                ...state,
                settings: update(state.settings, {$merge: {pageZoom: action.payload}})
            }
        }
        case actionTypes.SET_BOOKMARKS_SHOW_STATUS: {
            return {
                ...state,
                settings: update(state.settings, {$merge: {isBookmarksShow: action.payload}})
            }
        }
        case actionTypes.SET_DEFAULT_SETTINGS: {
            return {
                ...state,
                settings: state.defaultSettings
            }
        }
        case actionTypes.SET_SPINNER_STATUS: {
            return {
                ...state,
                settings: update(state.settings, {$merge: {isLoading: action.payload}})
            }
        }
        default:
            return state;
    }

};

export default reducer;
