import * as actionTypes from 'store/actions/actions';

const initialState = {
    people: [
        {
            name: 'Vlad',
            email: 'vlad@gmail.com',
            image: '/img/default-image-1.jpg'
        }
    ]
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_PERSON: {
            return {
                ...state,
                people: state.people.concat([action.payload])
            }
        }
        case actionTypes.EDIT_PERSON: {
            return {
                ...state,
                people: state.people.map((person, index) => index === action.payload.index ? {
                    name: action.payload.name,
                    email: action.payload.email,
                    image: action.payload.image
                } : person)
            }
        }
        case actionTypes.REMOVE_PERSON: {
            return {
                ...state,
                people: state.people.filter((person, index) => index !== action.payload)
            }
        }
        default:
            return state;
    }

};

export default reducer;
