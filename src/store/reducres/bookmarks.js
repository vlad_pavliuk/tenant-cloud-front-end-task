import * as actionTypes from 'store/actions/actions';

const initialState = {
    bookmarks: [],
    searchText: '',
    matchedBookmarks: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_BOOKMARK: {
            return {
                ...state,
                bookmarks: state.bookmarks.concat([action.payload])
            }
        }
        case actionTypes.EDIT_BOOKMARK: {
            return {
                ...state,
                bookmarks: state.bookmarks.map((bookmark, index) => index === action.payload.index ? {
                    title: action.payload.title,
                    url: action.payload.url
                } : bookmark)
            }
        }
        case actionTypes.REMOVE_BOOKMARK: {
            return {
                ...state,
                bookmarks: state.bookmarks.filter((bookmark, index) => index !== action.payload)
            }
        }
        case actionTypes.SEARCH_BOOKMARKS: {
            return {
                ...state,
                matchedBookmarks: action.payload
                    ? state.bookmarks.filter(bookmark => bookmark.title.toLowerCase().match(action.payload.toLowerCase()))
                    : [],
                searchText: action.payload
            }
        }
        case actionTypes.GET_BOOKMARKS: {
            return {
                ...state,
                bookmarks: state.bookmarks.concat(
                    action.payload.filter(
                        storeBookmark => !state.bookmarks.find(bookmark => storeBookmark.title !== bookmark.title)
                    )
                )
            }
        }
        case actionTypes.SAVE_BOOKMARKS: {
            let bookmarks = JSON.parse(localStorage.getItem('bookmarks')) || [];

            bookmarks = bookmarks.concat(
                action.payload.filter(
                    storeBookmark => !bookmarks.find(bookmark => storeBookmark.title !== bookmark.title)
                )
            );

            localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
            return state;
        }
        default:
            return state;
    }

};

export default reducer;
