import {bookmarksRef} from "../../config/firebase";

export const ADD_BOOKMARK = 'ADD_BOOKMARK';
export const EDIT_BOOKMARK = 'EDIT_BOOKMARK';
export const REMOVE_BOOKMARK = 'REMOVE_BOOKMARK';
export const SEARCH_BOOKMARKS = 'SEARCH_BOOKMARKS';
export const GET_BOOKMARKS = 'GET_BOOKMARKS';
export const SAVE_BOOKMARKS = 'SAVE_BOOKMARKS';
export const ADD_PERSON = 'ADD_PERSON';
export const EDIT_PERSON = 'EDIT_PERSON';
export const REMOVE_PERSON = 'REMOVE_PERSON';
export const SET_THEME = 'SET_THEME';
export const SET_FONT_SIZE = 'SET_FONT_SIZE';
export const SET_PAGE_ZOOM = 'SET_PAGE_ZOOM';
export const SET_BOOKMARKS_SHOW_STATUS = 'SET_BOOKMARKS_SHOW_STATUS';
export const SET_DEFAULT_SETTINGS = 'SET_DEFAULT_SETTINGS';
export const SET_SPINNER_STATUS = 'SET_SPINNER_STATUS ';

export const getBookmarks = () => async dispatch => {
    dispatch({type: SET_SPINNER_STATUS, payload: true});
    bookmarksRef.on("value", snapshot => {
        dispatch({type: SET_SPINNER_STATUS, payload: false});
        dispatch({type: SAVE_BOOKMARKS, payload: snapshot.val()});
        dispatch({type: GET_BOOKMARKS, payload: snapshot.val()})
    });
};
