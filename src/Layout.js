import React from 'react';
import {Route, BrowserRouter, Switch, Redirect} from 'react-router-dom';
import WebsiteScene from './scenes/Website/WebsiteScene'
import Settings from './scenes/Settings/SettingsScene'
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import {connect} from 'react-redux';
import Spinner from './shared/components/Spinner'
import * as actionsTypes from 'store/actions/actions'

class Layout extends React.Component {

    constructor(props) {
        super(props);
        this.myRef = React.createRef();
    }

    state = {
        theme: createMuiTheme({
            typography: {
                fontSize: this.props.fontSize,
            },
        })
    };

    componentWillMount() {
        this.props.getBookmarks();
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.theme !== nextProps.theme || this.state.fontSize !== nextProps.fontSize) {
            this.setState({
                theme: createMuiTheme({
                    typography: {
                        fontSize: nextProps.fontSize,
                    },
                })
            });
        }
    }

    render() {
        return (
            <div>
                <MuiThemeProvider theme={this.state.theme}>
                    <BrowserRouter>
                        <div ref={this.myRef}>

                            <Spinner/>

                            <Switch>
                                {
                                    this.props.isBookmarksShow &&
                                    <Route path="/host" exact component={WebsiteScene}/>
                                }
                                <Route path="/host/settings" exact component={Settings}/>
                                <Redirect from="/" to="/host/settings"/>
                            </Switch>
                        </div>
                    </BrowserRouter>
                </MuiThemeProvider>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isBookmarksShow: state.settings.settings.isBookmarksShow,
        theme: state.settings.settings.theme,
        fontSize: state.settings.settings.fontSize
    }
};

const mapStateToDispatch = (dispatch) => {
    return {
        getBookmarks: () => dispatch(actionsTypes.getBookmarks()),
    }
};

export default connect(mapStateToProps, mapStateToDispatch)(Layout);