import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import bookmarksReducer from './store/reducres/bookmarks'
import peopleReducer from './store/reducres/people'
import settingsReducer from './store/reducres/settings'
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const rootReducer = combineReducers({
    bookmarks: bookmarksReducer,
    people: peopleReducer,
    settings: settingsReducer
});

const composerEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composerEnhancers(applyMiddleware(thunk)));

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
