import React from 'react';
import {connect} from "react-redux";
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemText';
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import List from '@material-ui/core/List';

import BookmarksModal from './components/BookmarksModal';
import TopBar from './components/TopBar';
import {Link} from 'react-router-dom';
import update from 'immutability-helper';
import * as actionsTypes from 'store/actions/actions'

class WebsiteScene extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            theme: createMuiTheme({
                palette: {
                    type: this.props.theme,
                },
                typography: {
                    fontSize: this.props.fontSize,
                },
            }),
            modal: {
                isOpen: false,
                title: '',
                onSubmit: null,
                onClose: this.closeModal,
                defaultValues: {
                    name: '',
                    email: '',
                    image: ''
                }
            }
        };
    }

    closeModal = () => {
        this.setState({
            modal: update(this.state.modal, {
                $merge: {
                    isOpen: false
                }
            })
        });
    };

    openEditBookmarkModel = (index, bookmark) => {
        this.setState({
            modal: update(this.state.modal, {
                $merge: {
                    isOpen: true,
                    title: `Edit ${bookmark.title}`,
                    onSubmit: this.props.editBookmark.bind(this, index),
                    defaultValues: {
                        title: bookmark.title,
                        url: bookmark.url
                    },
                    submitButton: 'edit'
                }
            })
        })
    };

    openAddBookmarkModel = () => {
        this.setState({
            modal: update(this.state.modal, {
                $merge: {
                    isOpen: true,
                    title: 'Add new bookmark',
                    onSubmit: this.props.addBookmark,
                    defaultValues: {
                        title: '',
                        url: ''
                    },
                    submitButton: 'add'
                }
            })
        });
    };

    handleRemoveButtonClick = (index) => {
        this.props.removeBookmark(index);
    };

    componentWillMount() {
        document.body.style.zoom = 100 * this.props.pageZoom + "%";
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.theme !== nextProps.theme || this.state.fontSize !== nextProps.fontSize) {
            this.setState({
                theme: createMuiTheme({
                    palette: {
                        type: nextProps.theme,
                    },
                    typography: {
                        fontSize: nextProps.fontSize,
                    },
                })
            });
        }

        if (this.props.pageZoom !== nextProps.pageZoom) {
            document.body.style.zoom = 100 * nextProps.pageZoom + "%";
        }
    }

    getBookmarksList() {
        let bookmarksListToRender = this.props.searchText ? this.props.matchedBookmarks : this.props.bookmarks;

        return bookmarksListToRender.map((bookmark, index) => (
            <ListItem key={index}>
                <ListItemText primary={
                    bookmark.url ?
                        <Button
                            variant="contained"
                            color="primary"
                            target="_blank"
                            component="a"
                            href={bookmark.url}
                        >{bookmark.title}</Button>
                        : bookmark.title
                }/>


                <ListItemSecondaryAction>
                    <div style={{float: 'right'}}>
                        <Button onClick={() => this.openEditBookmarkModel(index, bookmark)}

                                color="primary"
                        >
                            Edit
                        </Button>
                        <Button onClick={() => this.handleRemoveButtonClick(index)}
                                color="secondary"
                        >
                            Remove
                        </Button>
                    </div>
                </ListItemSecondaryAction>
            </ListItem>
        ));
    }

    render() {
        return (
            <MuiThemeProvider theme={this.state.theme}>
                <TopBar/>
                <Paper
                    square={true}
                    position="fixed"
                    className={'page-content'}
                >
                    <div>
                        <Link to={'/settings'} className="no-underlying">
                            <Button variant="contained">
                                To settings
                            </Button>
                        </Link>

                        <Grid container justify={'center'}>
                            <Grid item xl={6} lg={6} md={10} sm={10} xs={12}>
                                <Typography variant="body2" align={'left'} gutterBottom>
                                    Bookmarks list
                                </Typography>
                                <Card>
                                    {
                                        this.props.bookmarks.length > 0 &&
                                        <List>
                                            {this.getBookmarksList()}
                                        </List>
                                    }
                                    {
                                        this.props.bookmarks.length <= 0 &&
                                        <ListItem>
                                            <ListItemText primary={'No bookmarks'}/>
                                        </ListItem>
                                    }
                                </Card>
                            </Grid>
                        </Grid>

                        <Grid container justify={'center'}>
                            <Grid item xs={5}>
                                <Button color="primary"
                                        className={'website-scene-add-bookmark-button'}
                                        variant="contained"
                                        onClick={this.openAddBookmarkModel}>
                                    Create bookmark
                                </Button>
                            </Grid>
                        </Grid>

                    </div>
                    <BookmarksModal
                        modalTitle={this.state.modal.title}
                        open={this.state.modal.isOpen}
                        title={this.state.modal.defaultValues.title}
                        url={this.state.modal.defaultValues.url}
                        submitButton={this.state.modal.submitButton}
                        onClose={this.state.modal.onClose}
                        onSubmit={this.state.modal.onSubmit}
                    />
                    <div style={{paddingBottom: 81 + 'vh'}}></div>
                </Paper>
            </MuiThemeProvider>
        );
    }
}

const mapStateToProps = state => {
    return {
        bookmarks: state.bookmarks.bookmarks,
        matchedBookmarks: state.bookmarks.matchedBookmarks,
        searchText: state.bookmarks.searchText,
        theme: state.settings.settings.theme,
        fontSize: state.settings.settings.fontSize,
        pageZoom: state.settings.settings.pageZoom
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addBookmark: (payload) => dispatch({type: actionsTypes.ADD_BOOKMARK, payload}),
        editBookmark: (index, payload) => dispatch({type: actionsTypes.EDIT_BOOKMARK, payload: {...payload, index}}),
        removeBookmark: (payload) => dispatch({type: actionsTypes.REMOVE_BOOKMARK, payload}),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(WebsiteScene);
