import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Avatar from '@material-ui/core/Avatar';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import SearchIcon from '@material-ui/icons/Search';
import {connect} from 'react-redux';
import * as actionsTypes from 'store/actions/actions'

class TopBar extends React.Component {
    handleSearchInput = (event) => {
        let text = event.target.value;
        this.props.searchBookmarks(text);
    };

    render() {
        return (
            <AppBar color="primary">
                <Toolbar>
                    <Typography
                        variant="title"
                        color="inherit"
                    >
                        Website
                    </Typography>

                    <Grid container>
                        <Grid item xs={3}/>
                        <Grid item xs={5}>
                            <FormControl fullWidth>
                                <TextField
                                    onChange={this.handleSearchInput}
                                    className="settings-search-input"
                                    placeholder={'Search bookmarks'}
                                    InputProps={{
                                        style: {color: 'white'},
                                        disableUnderline: true,
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                {
                                                    this.props.people[0] && this.props.people[0].image &&
                                                    <Avatar
                                                        alt="Remy Sharp"
                                                        src={this.props.people[0].image}
                                                    />
                                                }
                                                <SearchIcon/>
                                            </InputAdornment>
                                        )
                                    }}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </FormControl>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        people: state.people.people
    }
};

const mapStateToDispatch = (dispatch) => {
    return {
        searchBookmarks: (payload) => dispatch({type: actionsTypes.SEARCH_BOOKMARKS, payload})
    }
};

export default connect(mapStateToProps, mapStateToDispatch)(TopBar);
