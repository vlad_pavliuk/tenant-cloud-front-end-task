import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import TextField from '@material-ui/core/TextField';
import validUrl from 'valid-url';
import update from 'immutability-helper';

class BookmarksModal extends React.Component {

    state = {
        title: '',
        url: '',
    };

    handleChangeBookmarkTitleEvent = (event) => {
        let title = event.target.value;

        this.setState((state) => ({
            ...state,
            title
        }));
    };

    handleChangeBookmarkUrlEvent = (event) => {
        let url = event.target.value;

        this.setState((state) => ({
            ...state,
            url
        }));
    };

    validateForm = () => {
        return this.state.title && validUrl.isUri(this.state.url);
    };

    handleSubmitEvent = () => {
        if (this.validateForm()) {
            this.props.onSubmit({title: this.state.title, url: this.state.url});
            this.setState({
                title: '',
                url: ''
            });
            this.props.onClose();
        }
    };

    componentWillReceiveProps(nextProps) {
        if (this.state.title !== nextProps.title && this.state.url !== nextProps.url) {
            this.setState(update(this.state, {
                $merge: {
                    title: nextProps.title,
                    url: nextProps.url
                }
            }));
        }
    }

    render() {
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={this.props.onClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">{this.props.modalTitle}</DialogTitle>
                    <DialogContent>
                        <TextField
                            onChange={event => this.handleChangeBookmarkTitleEvent(event)}
                            margin="dense"
                            value={this.state.title}
                            label="Bookmark title"
                            fullWidth
                        />
                        <TextField
                            onChange={event => this.handleChangeBookmarkUrlEvent(event)}
                            margin="dense"
                            label="Bookmark URL"
                            value={this.state.url}
                            fullWidth
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.props.onClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSubmitEvent} color="primary">
                            {this.props.submitButton}
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default BookmarksModal;
