import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Switch from '@material-ui/core/Switch';
import {connect} from 'react-redux';
import * as actionsTypes from 'store/actions/actions'

class ShowBookmarks extends React.Component {
    handleChange = (event) => {
        this.props.setBookmarkShowStatus(event.target.checked);
    };

    render() {
        return (
            <ListItem>
                <ListItemText primary={'Show bookmarks'}/>
                <ListItemSecondaryAction>
                    <Switch
                        onChange={this.handleChange}
                        checked={this.props.isBookmarksShow}
                    />
                </ListItemSecondaryAction>
            </ListItem>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isBookmarksShow: state.settings.settings.isBookmarksShow
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setBookmarkShowStatus: (payload) => dispatch({type: actionsTypes.SET_BOOKMARKS_SHOW_STATUS, payload})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowBookmarks);
