import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import {connect} from 'react-redux';
import * as actionsTypes from 'store/actions/actions'

class PageZoom extends React.Component {
    state = {
        pageZooms: [
            {
                title: '25%',
                zoom: .25
            },
            {
                title: '33%',
                zoom: .33
            },
            {
                title: '50%',
                zoom: .5
            },
            {
                title: '67%',
                zoom: .67
            },
            {
                title: '75%',
                zoom: .75
            },
            {
                title: '80%',
                zoom: .8
            },
            {
                title: '90%',
                zoom: .9
            },
            {
                title: '100%',
                zoom: 1
            },
            {
                title: '110%',
                zoom: 1.1
            },
            {
                title: '125%',
                zoom: 1.25
            },
            {
                title: '150%',
                zoom: 1.5
            },
            {
                title: '175%',
                zoom: 1.75
            },
            {
                title: '200%',
                zoom: 2
            },
            {
                title: '250%',
                zoom: 2.5
            },
            {
                title: '300%',
                zoom: 3
            },
            {
                title: '400%',
                zoom: 4
            },
            {
                title: '500%',
                zoom: 5
            }
        ]
    };

    getPageZoomsList = () => {
        return this.state.pageZooms.map((font, index) => (
            <MenuItem key={index} value={font.zoom}>
                {font.title}
            </MenuItem>
        ));
    };

    onChangeEvent = (event) => {
        let size = event.target.value;
        this.props.onPageZoomChange(size);
    };
    //
    // componentWillReceiveProps(nextProps) {
    //     document.body.style.zoom = 100 * nextProps.pageZoom + "%";
    // }

    render() {
        return (
            <ListItem>
                <Grid container>
                    <Grid item xs={8}>
                        <ListItemText primary={'Page zoom'}/>
                    </Grid>
                    <Grid item xs={4}>
                        <FormControl fullWidth>
                            <TextField
                                select
                                onChange={this.onChangeEvent}
                                value={this.props.pageZoom}
                            >
                                {this.getPageZoomsList()}
                            </TextField>
                        </FormControl>
                    </Grid>
                </Grid>
            </ListItem>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        pageZoom: state.settings.settings.pageZoom
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onPageZoomChange: (payload) => dispatch({type: actionsTypes.SET_PAGE_ZOOM, payload})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PageZoom);
