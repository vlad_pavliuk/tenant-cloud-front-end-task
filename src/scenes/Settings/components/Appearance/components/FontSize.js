import React from 'react'
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import {connect} from 'react-redux';
import * as actionsTypes from 'store/actions/actions'

class FontSize extends React.Component {
    state = {
        fontSizes: [
            {
                title: 'Very small',
                size: 10
            },
            {
                title: 'Small',
                size: 12
            },
            {
                title: 'Medium (Recommended)',
                size: 14
            },
            {
                title: 'Large',
                size: 16
            },
            {
                title: 'Very large',
                size: 18
            },
        ]
    };

    onChangeEvent = (event) => {
        let size = event.target.value;
        this.props.onSetFontSize(size);
    };

    getFontSizesList = () => {
        return this.state.fontSizes.map((font, index) => (
            <MenuItem key={index} value={font.size}>
                {font.title}
            </MenuItem>
        ));
    };

    render() {
        return (
            <ListItem>
                <Grid container>
                    <Grid item xs={8}>
                        <ListItemText primary={'Font size'}/>
                    </Grid>
                    <Grid item xs={4}>
                        <FormControl fullWidth>
                            <TextField
                                select
                                onChange={this.onChangeEvent}
                                value={this.props.fontSize}
                            >
                                {this.getFontSizesList()}
                            </TextField>
                        </FormControl>
                    </Grid>
                </Grid>
            </ListItem>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        fontSize: state.settings.settings.fontSize
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onSetFontSize: (payload) => dispatch({type: actionsTypes.SET_FONT_SIZE, payload})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FontSize);
