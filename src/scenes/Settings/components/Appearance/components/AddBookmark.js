import React from 'react'
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import {connect} from 'react-redux';
import * as actionsTypes from 'store/actions/actions'

class AddBookmark extends React.Component {
    state = {
        title: ''
    };

    getBookmarksList = () => {
        return this.props.bookmarks.map((bookmark, index) => (
            <ListItem key={index}>
                <ListItemText primary={bookmark.title}/>
            </ListItem>
        ));
    };

    validateTextInput = () => {
        return !!this.state.title;
    };

    handleAddButtonClick = () => {
        if (this.validateTextInput()) {
            this.props.addBookmark({title: this.state.title});
            this.setState({title: ''})
        }
    };

    handleTextInput = (event) => {
        let title = event.target.value;
        this.setState({title})
    };

    render() {
        return (
            <ListItem>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography variant="subheading">
                            Add bookmark
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <List component="nav">
                            {this.getBookmarksList()}
                        </List>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container
                              alignItems={'flex-end'}
                              spacing={16}
                        >
                            <Grid item xs={8}>
                                <FormControl fullWidth>
                                    <TextField
                                        label="Title"
                                        value={this.state.title}
                                        onChange={this.handleTextInput}
                                    />
                                </FormControl>

                            </Grid>
                            <Grid item xs={4}>
                                <Button
                                    onClick={this.handleAddButtonClick}
                                    variant="contained"
                                    color="primary"
                                >
                                    Add
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </ListItem>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        bookmarks: state.bookmarks.bookmarks
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addBookmark: (payload) => dispatch({type: actionsTypes.ADD_BOOKMARK, payload})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddBookmark);
