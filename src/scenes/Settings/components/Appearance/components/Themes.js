import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import Grid from '@material-ui/core/Grid';
import {connect} from 'react-redux';
import * as actionsTypes from 'store/actions/actions'

class Themes extends React.Component {

    onChangeValue = (value) => {
        this.props.onThemeChange(value);
    };

    render() {
        return (
            <ListItem>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography variant="subheading">
                            Themes
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container>
                            <Grid item xs={2}>
                                <FormControlLabel label="White" control={
                                    <Radio
                                        checked={this.props.theme === 'light'}
                                        onChange={() => this.onChangeValue('light')}
                                    />
                                }/>
                            </Grid>
                            <Grid item xs={10}>
                                <FormControlLabel label="Black" control={
                                    <Radio
                                        checked={this.props.theme === 'dark'}
                                        onChange={() => this.onChangeValue('dark')}
                                    />
                                }/>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </ListItem>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        theme: state.settings.settings.theme
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onThemeChange: (payload) => dispatch({type: actionsTypes.SET_THEME, payload})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Themes);
