import React from 'react';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';

import Themes from './components/Themes';
import AddBookmark from './components/AddBookmark';
import ShowBookmarks from './components/ShowBookmarks';
import FontSize from './components/FontSize';
import PageZoom from './components/PageZoom';
import {connect} from 'react-redux';

class Appearance extends React.Component {

    render() {
        return (
            <div className="settings-single-block">
                <Typography variant="body2" align={'left'} gutterBottom>
                    Appearance
                </Typography>

                <Card>
                    <List>
                        <Themes/>
                        <Divider/>
                        {this.props.isBookmarksShow && <div><AddBookmark/><Divider/></div>}
                        <ShowBookmarks/>
                        <Divider/>
                        <FontSize/>
                        <Divider/>
                        <PageZoom/>
                    </List>
                </Card>
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        isBookmarksShow: state.settings.settings.isBookmarksShow
    }
};

export default connect(mapStateToProps)(Appearance);
