import React from 'react';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import {connect} from 'react-redux';
import * as actionsTypes from '../../../store/actions/actions';

class Reset extends React.Component {
    handleRestButtonClick = () => {
        this.props.setDefaultSettings();
    };

    render() {
        return (
            <div className="settings-single-block" id="reset-block">
                <Typography variant="body2" align={'left'} gutterBottom>
                    Reset
                </Typography>
                <Card>
                    <List>
                        <ListItem>
                            <ListItemText
                                primary={<Button onClick={this.handleRestButtonClick} color="primary">Reset</Button>}
                                secondary={'Restore settings to their original defaults'}
                            />
                        </ListItem>
                    </List>
                </Card>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setDefaultSettings: () => dispatch({type: actionsTypes.SET_DEFAULT_SETTINGS})
    }
};

export default connect(null, mapDispatchToProps)(Reset);
