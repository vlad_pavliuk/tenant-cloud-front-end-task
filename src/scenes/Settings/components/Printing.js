import React from 'react';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';

class Printing extends React.Component {
    handleButtonClick = () => {
        window.print();
    };

    render() {
        return (
            <div className="settings-single-block">
                <Typography variant="body2" align={'left'} gutterBottom>
                    Printing
                </Typography>
                <Card>
                    <List>
                        <ListItem>
                            <Button onClick={this.handleButtonClick} color="primary">
                                Printers
                            </Button>
                        </ListItem>
                    </List>
                </Card>
            </div>
        );
    }
}

export default Printing;
