import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import TextField from '@material-ui/core/TextField';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Typography from '@material-ui/core/Typography';
import emailValidator from 'email-validator';
import update from 'immutability-helper';
import {connect} from 'react-redux';

class PeopleModal extends React.Component {
    state = {
        person: {
            name: '',
            email: '',
            image: ''
        }
    };

    componentWillReceiveProps(nextProps) {
        if (this.state.person.name !== nextProps.name && this.state.person.email !== nextProps.email) {
            this.setState({
                person: update(this.state.person, {
                    $merge: {
                        name: nextProps.name,
                        email: nextProps.email,
                        image: nextProps.image
                    }
                })
            });
        }
    }

    onChangeUserNameEvent(event) {
        let name = event.target.value;

        this.setState({
            person: update(this.state.person, {
                $merge: {
                    name
                }
            })
        });
    }

    onChangeUserEmailEvent(event) {
        let email = event.target.value;

        this.setState({
            person: update(this.state.person, {
                $merge: {
                    email
                }
            })
        });
    }

    handleImageClick = (image) => {
        this.setState({
            person: update(this.state.person, {
                $merge: {
                    image,
                }
            })
        });
    };

    validateForm = () => {
        return this.state.person.name && emailValidator.validate(this.state.person.email) && this.state.person.image;
    };

    onSubmitEvent = () => {
        if (this.validateForm()) {
            this.props.onSubmit({name: this.state.person.name, email: this.state.person.email, image: this.state.person.image});
            this.setState({
                person: {
                    name: '',
                    email: '',
                    image: ''
                }
            });
            this.props.onClose();
        }
    };

    getDefaultPersonImagesList = () => {
        return this.props.defaultPersonImagesUrls.map((imageUrl, index) => (
            <GridListTile
                onClick={() => this.handleImageClick(imageUrl)}
                className={'avatar-img' + (this.state.person.image === imageUrl ? ' selected-avatar-img' : '')}
                key={index}
            >
                <img src={imageUrl} alt={imageUrl}/>
            </GridListTile>
        ));
    };

    render() {
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={this.props.onClose}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">{this.props.title}</DialogTitle>
                    <DialogContent>
                        <TextField
                            onChange={event => this.onChangeUserNameEvent(event)}
                            margin="dense"
                            value={this.state.person.name}
                            label="User name"
                            fullWidth
                        />
                        <TextField
                            onChange={event => this.onChangeUserEmailEvent(event)}
                            margin="dense"
                            value={this.state.person.email}
                            label="User email"
                            type="email"
                            fullWidth
                        />
                        <Typography variant="title" gutterBottom>
                            Select profile avatar
                        </Typography>
                        <GridList cellHeight={84} spacing={20} cols={5}>
                            {this.getDefaultPersonImagesList()}
                        </GridList>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.props.onClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.onSubmitEvent} color="primary">
                            {this.props.submitButton}
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        defaultPersonImagesUrls: state.settings.defaultPersonImagesUrls
    }
};

export default connect(mapStateToProps)(PeopleModal);
