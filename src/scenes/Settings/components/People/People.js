import React from 'react'
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import PlayArrowIcon from '@material-ui/icons/PlayArrow'
import {connect} from 'react-redux';
import update from 'immutability-helper';
import * as actionsTypes from 'store/actions/actions'

import PeopleModule from './components/PeopleModal'

class People extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            modal: {
                isOpen: false,
                title: '',
                onSubmit: null,
                onClose: this.closeModal,
                defaultValues: {
                    name: '',
                    email: '',
                    image: ''
                }
            }
        };

        this.closeModal = this.closeModal.bind(this);
    }

    closeModal = () => {
        this.setState({
            modal: update(this.state.modal, {
                $merge: {
                    isOpen: false
                }
            })
        });
    };

    openEditPersonModel = (index, person) => {
        this.setState({
            modal: update(this.state.modal, {
                $merge: {
                    isOpen: true,
                    title: `Edit ${person.name}`,
                    onSubmit: this.props.editPerson.bind(this, index),
                    defaultValues: {
                        name: person.name,
                        email: person.email,
                        image: person.image
                    },
                    submitButton: 'edit'
                }
            })
        })
    };

    openAddPersonModel = () => {
        this.setState({
            modal: update(this.state.modal, {
                $merge: {
                    isOpen: true,
                    title: 'Add new person',
                    onSubmit: this.props.addPerson,
                    defaultValues: {
                        name: '',
                        email: '',
                        image: ''
                    },
                    submitButton: 'add'
                }
            })
        });
    };

    handleRemoveButtonClick = (index) => {
        this.props.removePerson(index)
    };

    getPeopleList() {
        return this.props.people.map((person, index) => (
            <ListItem key={index}>
                <Avatar alt="Remy Sharp" src={person.image}/>
                <ListItemText
                    primary={person.name}
                    secondary={person.email}
                />

                <ListItemSecondaryAction>
                    <Button
                        onClick={() => this.openEditPersonModel(index, person)}
                        color="primary"
                    >
                        Edit
                    </Button>
                    {
                        index !== 0 &&
                        <Button
                            onClick={() => this.handleRemoveButtonClick(index)}
                            color="secondary"
                        >
                            Remove
                        </Button>
                    }
                </ListItemSecondaryAction>
            </ListItem>
        ));
    }

    render() {
        return (
            <div className="settings-single-block">
                <Typography variant="body2" align={'left'} gutterBottom>
                    People
                </Typography>
                <Card>
                    <List>
                        {this.getPeopleList()}
                        <Divider/>
                        <ListItem onClick={this.openAddPersonModel} button>
                            <ListItemText primary={'Manage other people'}/>

                            <ListItemSecondaryAction>
                                <PlayArrowIcon/>
                            </ListItemSecondaryAction>
                        </ListItem>
                    </List>
                </Card>

                <PeopleModule
                    title={this.state.modal.title}
                    open={this.state.modal.isOpen}
                    name={this.state.modal.defaultValues.name}
                    email={this.state.modal.defaultValues.email}
                    image={this.state.modal.defaultValues.image}
                    submitButton={this.state.modal.submitButton}
                    onClose={this.state.modal.onClose}
                    onSubmit={this.state.modal.onSubmit}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        people: state.people.people
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addPerson: (payload) => dispatch({type: actionsTypes.ADD_PERSON, payload}),
        editPerson: (index, payload) => dispatch({type: actionsTypes.EDIT_PERSON, payload: {...payload, index}}),
        removePerson: (payload) => dispatch({type: actionsTypes.REMOVE_PERSON, payload})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(People);
