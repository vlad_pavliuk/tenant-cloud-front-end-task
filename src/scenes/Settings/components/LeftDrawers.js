import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import LocalPrintshopIcon from '@material-ui/icons/LocalPrintshop';
import RestoreIcon from '@material-ui/icons/Restore';
import PersonIcon from '@material-ui/icons/Person';
import ColorLensIcon from '@material-ui/icons/ColorLens';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {goToAnchor} from 'react-scrollable-anchor'
import {configureAnchors} from 'react-scrollable-anchor'

const styles = {
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
};

class LeftDrawers extends React.Component {
    state = {
        open: false
    };

    componentWillMount() {
        configureAnchors({offset: -70, scrollDuration: 200});
    }

    toggleDrawer = (side, open) => () => {
        this.setState({
            [side]: open,
        });
    };

    handleClick = () => {
        this.setState(state => ({open: !state.open}));
    };

    handleNavigationLinkClick = (sectionId) => {
        goToAnchor(sectionId);
    };

    render() {
        const {classes} = this.props;

        const sideList = (
            <div className={classes.list}>
                <List component="nav">
                    <ListItem>
                        <ListItemText primary="Settings"/>
                    </ListItem>
                </List>
                <Divider/>
                {
                    this.props.isBookmarksShow &&
                    <ListItem button>
                        <Link className="no-underlying" to={'/host'} tag={'button'}>
                            <ListItemText primary="Website page"/>
                        </Link>
                    </ListItem>
                }
                <ListItem button onClick={() => this.handleNavigationLinkClick('people-section')}>
                    <ListItemIcon>
                        <PersonIcon/>
                    </ListItemIcon>
                    <ListItemText primary="People"/>
                </ListItem>
                <ListItem button onClick={() => this.handleNavigationLinkClick('appearance-section')}>
                    <ListItemIcon>
                        <ColorLensIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Appearance"/>
                </ListItem>
                <Divider/>
                <ListItem button onClick={this.handleClick}>
                    <ListItemText primary="Advanced"/>
                    {this.state.open ? <ExpandLess/> : <ExpandMore/>}
                </ListItem>
                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItem button onClick={() => this.handleNavigationLinkClick('printing-section')}>
                            <ListItemIcon>
                                <LocalPrintshopIcon/>
                            </ListItemIcon>
                            <ListItemText primary="Printing"/>
                        </ListItem>
                        <ListItem button onClick={() => this.handleNavigationLinkClick('reset-section')}>
                            <ListItemIcon>
                                <RestoreIcon/>
                            </ListItemIcon>
                            <ListItemText primary="Reset"/>
                        </ListItem>
                    </List>
                </Collapse>
            </div>
        );

        return (
            <Drawer open={this.props.open}
                    onClose={this.props.onClose}>
                <div
                    tabIndex={0}
                    role="button"
                    onClick={this.toggleDrawer('left', false)}
                    onKeyDown={this.toggleDrawer('left', false)}
                >
                    {sideList}
                </div>
            </Drawer>
        );
    }
}

LeftDrawers.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
        isBookmarksShow: state.settings.settings.isBookmarksShow
    }
};

export default connect(mapStateToProps)(withStyles(styles)(LeftDrawers));
