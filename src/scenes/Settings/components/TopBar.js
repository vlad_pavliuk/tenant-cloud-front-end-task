import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

const TopBar = (props) => {
    return (
        <AppBar color="primary">
            <Toolbar>
                <IconButton
                    color="inherit"
                    aria-label="Menu"
                    onClick={() => props.onClick()}
                >
                    <MenuIcon/>
                </IconButton>
                <Typography variant="title" color="inherit">
                    Settings part
                </Typography>
            </Toolbar>

        </AppBar>
    );
};

export default TopBar;
