import React from 'react'
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import LeftDrawers from './components/LeftDrawers';
import TopBar from './components/TopBar';
import Appearance from './components/Appearance/AppearanceListContainer'
import People from './components/People/People'
import Printing from './components/Printing'
import Reset from './components/Reset'
import ScrollableAnchor from 'react-scrollable-anchor'

class SettingsScene extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLeftDrawerOpen: false,
            value: 'white'
        };
    }

    componentWillMount() {
        document.body.style.zoom = 100 + "%";
    }

    handleOpenDrower = () => {
        this.setState({isLeftDrawerOpen: true})
    };

    handleCloseDrower = () => {
        this.setState({isLeftDrawerOpen: false})
    };

    render() {
        return (
            <div>
                <TopBar onClick={this.handleOpenDrower}/>

                <Paper
                    square={true}
                    position="fixed"
                    className={'page-content'}
                >

                    <LeftDrawers onClose={this.handleCloseDrower}
                                 open={this.state.isLeftDrawerOpen}/>
                    <Grid container justify={'center'}>
                        <Grid item xl={6} lg={6} md={10} sm={10} xs={12}>
                            <ScrollableAnchor id={'people-section'}>
                                <People/>
                            </ScrollableAnchor>

                            <ScrollableAnchor id={'appearance-section'}>
                                <Appearance/>
                            </ScrollableAnchor>

                            <Typography variant="title" gutterBottom>
                                Advanced
                            </Typography>
                            <ScrollableAnchor id={'printing-section'}>
                                <Printing/>
                            </ScrollableAnchor>
                            <ScrollableAnchor id={'reset-section'}>
                                <Reset/>
                            </ScrollableAnchor>
                        </Grid>
                    </Grid>
                    <div style={{paddingBottom: 75 + 'vh'}}></div>
                </Paper>
            </div>

        );
    }
}

export default SettingsScene;
